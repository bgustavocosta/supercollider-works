# supercollider-works

Basic supercollider scratchpad for some Synthdefs and some quick functions
to test out UGEN's

There is a goal to create a template scd file that allows for livecoding
and is properly created to support cross platform (GNU/Linux and macOS) use

**NB:** the `partconv-example.scd` file is not mine, and i don't know where it is from
