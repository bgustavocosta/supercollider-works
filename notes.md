# Notes

## The SCLOrk SynthDef standards

SynthDefs should have the following arguments (default values encouraged but flexible):

```
out = 0, to be used in Out.ar(out, ...)
pan = 0, to be used in Out.ar(out, Pan2.ar(snd, pan))
freq = 440, for pitched synths
amp = 0.2, for global amp of synth
att = 0.01, attack time of the main amplitude envelope
rel = 1, release time of amplitude envelope
sus = 1, sustain level (as in a typical ADSR)
dec = 0.1, decay time (as in typical ADSR)
gate = 1, if using envelopes like ADSR (not needed if using self terminating envs such as Env.perc)
```
* Use variables snd for main sound (whatever that is) and env for amplitude env. Variations as needed.
* Use doneAction: 2 to make synth free itself after note is done.
* Avoid specifying durations directly inside SynthDef (other than att and rel) -- patterns will take care of durations.
* Keep any relevant comments about the SynthDef as the header of the file (use block comment)

Above taken from:
https://github.com/SCLOrkHub/SCLOrkSynths

## Envelope curves:

|                               |      |                                                                                                                 |
|:------------------------------|:-----|:----------------------------------------------------------------------------------------------------------------|
| \step                         |      | flat segments (immediately jumps to final value)
| \hold                         |      | flat segments (holds initial value, jump to final value at the end of the segment)
| \linear                       | \lin | linear segments, the default
| \exponential                  | \exp | natural exponential growth and decay. In this case, the levels must all be nonzero and have the same sign.
| \sine                         | \sin | sinusoidal S shaped segments.
| \welch                        | \wel | sinusoidal segments shaped like the sides of a Welch window.
| \squared                      | \sqr | squared segment
| \cubed                        | \cub | cubed segment
| a Float                       |      | a curvature value for all segments. 0 means linear, positive and negative numbers curve the segment up and down.
| an Array of symbols or floats |      | curvature values for each segment.

## Useful links to check

check here the sachiko and tsunoya
https://github.com/schollz/18000

Also worth checking:
https://github.com/theseanco/howto_co34pt_liveCode/

