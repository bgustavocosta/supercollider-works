// TODO: This is untested
SynthDef(\dchum, {
  arg out = 0,
      freq = 12000,
      pan = 0,
      amp = 0.2,
      gate = 1,
      att = 0.001,
      rel = 1;

  var sig;

  sig = HPF.ar(Pulse.ar(60, mul: 0.5), freq);

  sig = sig * EnvGen.kr(Env.asr(att, 1, rel).kr(gate: gate, doneAction: 2));

  sig = Pan2.ar(sig, pan, amp);
 
  Out.ar(out, sig);

}).add;

// Original scratch:
{ HPF.ar(Pulse.ar(60, mul: 0.5)!2,MouseX.kr(20, 12000, 1)) }.play;

